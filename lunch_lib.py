D=1
import numpy as np
from datetime import datetime
import pprint
def cook(n, quality_p, D):
    return (n/2)*D*quality_p
def clean(n, dirty_p, D):
    return (n/2)*D*dirty_p
def booze(n, D):
    return (n/4)*D
def eat(n, quality_p, dirty_p, D):
    return -((n/2)*D*(quality_p+dirty_p))/n
    
def score_update(people, people_eating, cleaner, chef, alcoholics,  quality_p, dirty_p, D):
    n=len(people_eating)
    
        
    people[cleaner]['score']+= eat(n, quality_p, dirty_p, D) + clean(n, dirty_p, D)
    people[cleaner]['meals']+=1
    people[cleaner]['cleanings']+=1
    
    people[chef]['score']+= eat(n, quality_p, dirty_p, D)
    people[chef]['meals']+=1
    people[chef]['cookings']+=1
    
    if cleaner==chef:
        people[chef]['score']+=abs(eat(n, quality_p, dirty_p, D))
                          
    for person in people_eating:
        if person==chef or person==cleaner:
            continue
        people[person]['score']+=eat(n, quality_p, dirty_p, D)
        people[person]['meals']+=1
    n_giri=len(alcoholics)
    for i in range(n_giri):
        if len(alcoholics[i])>0:
            for alcoholic in alcoholics[i]:
                people[alcoholic]['score']+=booze(n, D)
                people[alcoholic]['pending_boozes']+=1
                people[alcoholic]['pending_from']=datetime.now()
            
            for person in people_eating:
                if person!= alcoholic:
                    people[person]['score']-=booze(n,D)/(n-1)
def init():
    people=dict()
    people_list=["ambarabauce", "boss", "vannico", "limone", "justin", 
             "tacchino", "sebba", "pino", "avvisatina", "gianni", 
             "valerione", "compagnogino", "fede", "camilo", "pippo", 
             "valery", "lucchetto", "scurcio", "dicarlo", "padi"]

    for person in people_list:
        people[person]={'score':0,'meals':0, 'cleanings':0, 'cookings':0 , 'boozes':0, 
                    'pending_boozes':0, 'pending_from': 0}
    
    
    people['pippo']['score']=0.01
    people['compagnogino']['score']=0.02
    people['fede']['score']=0.03   
    people['limone']['score']=0.04    
    people['valerione']['score']=0.5    
    people['pino']['score']=0.06    
    people['boss']['score']=0.07    
    people['vannico']['score']=0.08
    people['camilo']['score']=0.09
    people['gianni']['score']=0.10
    people['ambarabauce']['score']=0.11
    people['justin']['score']=0.12
    people['sebba']['score']=0.13
    people['tacchino']['score']=0.14   
    
    people['sebba']['pending_boozes']=1
    people['sebba']['pending_from']=datetime(2019, 2, 13)
    
    return people
    
    

def dishwasher(people, people_eating, chef, alcoholics, quality_p):
    #dw_score=999
    #dw=''
    score_and_people=[]
    n_giri=len(alcoholics)
    people[chef]['score']+=cook(len(people_eating), quality_p, 1) 
    for person in people_eating:
        entry=[]
        entry.append(people[person]['score'])
        entry.append(person)
        score_and_people.append(entry)
    score_and_people.sort()
    for i in range(n_giri):
        n_alcoholics=len(alcoholics[i])
        for j in range(n_alcoholics):
            if score_and_people[0][1] in alcoholics[i]:
                score_and_people.append(score_and_people.pop(0))
    
    
    return score_and_people[0][1]
    
def new_person(people, name):
    people[name]={'score':0,'meals':0, 'cleanings':0, 'cookings':0 , 'boozes':0}
def write_stats(people_eating, chef, cleaner, alcoholics, quality_p=1, dirty_p=1):
    stats_file=open('stats.txt', 'a+')
    stats_file.write(str(datetime.now()) + '\n'+ 
                'commensali: '+ str(people_eating) + '\n' +
                'chef: ' + chef + '\n'+
                'cleaner: '+ cleaner + '\n' +
                'alcoholics: ' + str(alcoholics)+'\n'+
                'quality parameter: ' +str(quality_p)+'\n'+
                'dirty parameter: '+str(dirty_p)+'\n'+
                '\n')
    stats_file.close()
def check_chef(people, chef):
    max_score=-999
    flag=0
    for person in people:
        if people[chef]['score']< people[person]['score']:
            print('Lo chef ha il permesso di cucinare!')
            flag=1
            break
    if flag!=1:
        for person in people :
            if people[person]['score']>max_score and person!=chef:
                max_score=people[person]['score']
                second_max_person=person
        if people[chef]['score']-people[second_max_person]['score']>2*D:
            print("Chef c'hai troppo score! "+ second_max_person+ " c'ha "+ str(people[second_max_person]['score'])+
                  ", tu "+ str(people[chef]['score']))
        else:
            print('Lo chef ha il permesso di cucinare!')
        
        
def check_names(people, people_to_check):
    flag=1
    for person in people_to_check:
        if person not in people:
            print("C'è qualche errore nei nomi delle persone. Arifai.")
            flag=0
    return flag
def check_alcoholics_names(people, alcoholics):
    flag=1
    n_giri=len(alcoholics)
    for i in range(n_giri):
        for person in alcoholics[i]:
            if person not in people:
                print("C'è qualche errore nei nomi delle persone. Arifai.")
                flag=0
    return flag

def check_name(people, name_to_check):
    flag=1
    if name_to_check not in people:
        print("C'è qualche errore nel nome. Arifai.")
        flag=0
    return flag
'''            
def check_booze_permission(people, people_to_check): 
    flag=1
    for person in people_to_check:
        if people[person]['pending_boozes']>=1:
            current_date=datetime.now()
            pending_from=current_date-people[person]['pending_from']
            print( person +" Hai già un amaro pending da "+ str(pending_from.days)+ " giorni . LAVA.")
            flag=0
            break
            
    return flag
'''
def reset_booze(people, people_to_reset):
    for person in people_to_reset:
        if people[person]['pending_boozes']>=1:
            people[person]['pending_boozes']-=1
            people[person]['boozes']+=1
            if people[person]['pending_boozes']==0:
                people[person]['pending_from']=0

def list_pending_booze(people):
    people_pending_booze=[]
    for person in people:
        if people[person]['pending_boozes']!=0:
            people_pending_booze.append(person)
    if len(people_pending_booze)!=0:
        print("Le persone con amaro pending sono: ")
        for person in people_pending_booze:
            print(person)
    else:
        print("Nessuno deve portare amari")
            
def add_person(people, name):
    people[name]={'score':0,'meals':0, 'cleanings':0, 'cookings':0 , 'boozes':0, 
                    'pending_boozes':0, 'pending_from': 0}
def list_people(people):
    for person in people:
        pprint.pprint(person)